Merci de suivre ce guide d'installation afin d'améliorer la gestion du parc informatique. 


# Installation de l'agent de gestion du parc informatique

## Si vous êtes sur Windows

Executez le fichier .exe

## Si vous êtes sur Ubuntu

Executez les commandes suivantes dans un terminal : 

```bash
sudo apt -y install dmidecode hwdata ucf hdparm
sudo apt -y install perl libuniversal-require-perl libwww-perl libparse-edid-perl
sudo apt -y install libproc-daemon-perl libfile-which-perl libhttp-daemon-perl
sudo apt -y install libxml-treepp-perl libyaml-perl libnet-cups-perl libnet-ip-perl
sudo apt -y install libdigest-sha-perl libsocket-getaddrinfo-perl libtext-template-perl
sudo apt -y install libxml-xpath-perl libyaml-tiny-perl
```

### Configuration 

- Faites un git clone du dépot GIT en local, puis allez dans le dossier. Ouvrez un terminal dans ce répertoire et lancez cette commande :
```bash
sudo dpkg -i ./fusioninventory-agent_2.5.2-1_all.deb
```

- Éditez le fichier de configuration : sudo nano /etc/fusioninventory/agent.cfg
A cette ligne : `server = http:///glpi/plugins/fusioninventory/`
  - Décommentez ( supprimez le # au début de la ligne ) et remplacez `glpi` par l'adresse IP du serveur : **192.168.0.120**.
- Lancez l'agent Fusion inventory : `systemctl restart fusioninventory-agent`
- Rechargez l'agent Fusion Inventory après avoir modifié le fichier de configuration : 
`systemctl reload fusioninventory-agent`
- Forcez l'inventaire : `sudo pkill -USR1 -f -P 1 fusioninventory-agent`
